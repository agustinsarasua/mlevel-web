import { Injectable } from "@angular/core";
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { RestService } from "./rest.service";
import { environment } from './../../environments/environment';

@Injectable()
export class FbBackendService {

  constructor(private http: Http, private restService: RestService) { }

  public indexToken(accessToken: string, fbUserId: string): Promise<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.restService.doPost<string>(environment.fbBackendUrl, "index?access_token="+accessToken+"&fb_user_id="+fbUserId, "", headers, true);
  }

}
