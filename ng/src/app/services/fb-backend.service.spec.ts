import { TestBed, inject } from '@angular/core/testing';

import { FbBackendService } from './fb-backend.service';

describe('FbBackendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FbBackendService]
    });
  });

  it('should ...', inject([FbBackendService], (service: FbBackendService) => {
    expect(service).toBeTruthy();
  }));
});
