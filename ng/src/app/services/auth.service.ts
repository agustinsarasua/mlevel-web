import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthInfo } from './../model/auth-info';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { AngularFire, AuthProviders, FirebaseAuthState } from 'angularfire2';

@Injectable()
export class AuthService {
  
  static UNKNOWN_USER = new AuthInfo(null);
  authInfo$: BehaviorSubject<AuthInfo> = new BehaviorSubject<AuthInfo>(AuthService.UNKNOWN_USER);

  constructor(private af: AngularFire) { }

  // logIn(email: string, password: string): Promise<FirebaseAuthState> {
  //   return this.af.auth.login({email: email, password: password});
  // }

  getAuthInfo(): Observable<AuthInfo> {
    return this.af.auth.map(auth => {
      if(auth) {
        console.log('got the uid');
        let authInfo = new AuthInfo(auth.uid);
        this.authInfo$.next(authInfo);
        return authInfo;
      } 
      else {
        this.authInfo$.next(AuthService.UNKNOWN_USER);
        return AuthService.UNKNOWN_USER;
      }
    });
  }
}
