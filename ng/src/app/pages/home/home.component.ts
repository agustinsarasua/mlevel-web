import { Component, OnInit } from '@angular/core';
import { EventService } from './../../services/event.service';
import { Event } from './../../model/event'
import { FacebookService, FacebookInitParams, FacebookLoginStatus } from 'ng2-facebook-sdk';
import { FbBackendService } from './../../services/fb-backend.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  events: any[] = [];
  num = 1;
  
  constructor(private eventService: EventService, private fbk: FacebookService, public fbBackendService: FbBackendService) {
    // for (this.num; this.num <= 21; this.num += 1) {
    //   this.events.push(this.num);
    // }
    this.fbk.getLoginStatus().then(
        (response: FacebookLoginStatus) => {
          console.log(response);
          this.fbBackendService.indexToken(response.authResponse.accessToken, response.authResponse.userID);
          //this.router.navigate(['/user/profile']);
        },
        (error: any) => console.error(error)
      );

      this.eventService.loadUserEvents().then(collectionResponse =>
      {
        console.log(collectionResponse);
        this.events = collectionResponse.items;
      });
  }

  ngOnInit() {
  }

}
