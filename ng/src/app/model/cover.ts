export class Cover {
  public id: string;
  public source: string;
  public offsetX: number;
  public offsetY: number;
}
