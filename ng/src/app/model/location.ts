export class Location {
  public city: string;
  public country: string;
  public latitude: number;
  public longitude: number;
  public street: string;
  public zip: string;

  constructor(fbLocation: any){
    this.city = fbLocation.city ? fbLocation.city : null;
    this.country = fbLocation.country != null ? fbLocation.country : null;
    this.latitude = fbLocation.latitude ? fbLocation.latitude : null;
    this.longitude = fbLocation.longitude ? fbLocation.longitude : null;
    this.street = fbLocation.street ? fbLocation.street : null;
    this.zip = fbLocation.zip ? fbLocation.zip : null;
  }
}
