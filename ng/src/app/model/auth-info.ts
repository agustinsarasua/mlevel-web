export class AuthInfo {
  public uid: string;

  isLoggedIn(): boolean {
    return this.uid != null;
  }

  constructor (uid: string) {
      this.uid = uid;
  }
}
