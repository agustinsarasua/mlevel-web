export class CollectionResponse<T> {

    public items: Array<T>;
}