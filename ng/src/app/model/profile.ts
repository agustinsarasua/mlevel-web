import {Location} from './location';
import {Cover} from './cover';

export class Profile {

    public id: string;
    public name: string;
    public lastName: string;
    public birthday: string;
    public email: string;
    public sex: string;

    constructor(name: string){
        this.name = name;
    }
}


    // @Id
    // private Long id;

    // @Parent
    // @ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
    // private Key<UserEntity> userEntity;

    // @Ignore
    // private String userId;

    // private String name;

    // private String lastName;

    // private String displayName;

    // private String shortMessage;

    // @Unindex(IfNull.class)
    // private Date birthday;

    // @Unindex(IfNull.class)
    // private String email;

    // private List<Category> categories;

    // private DiscoveryPreferences discoveryPreferences;

    // private String sex;