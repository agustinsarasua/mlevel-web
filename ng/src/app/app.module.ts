import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { JazzDialogComponent } from './material/dialog/dialog.component';
import { CalendarDialogComponent } from './apps/fullcalendar/fullcalendar.component';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';

import { AngularFireModule, AuthMethods, AuthProviders } from 'angularfire2';
import { FacebookService } from 'ng2-facebook-sdk';
import { environment } from '../environments/environment';

import { AuthService } from './services/auth.service'
import { AuthGuard } from './auth.guard'
import { FbBackendService } from './services/fb-backend.service';
import { EventService } from './services/event.service';
import { ProfileService } from './services/profile.service';
import { RestService } from './services/rest.service';
import { MockBackend } from '@angular/http/testing';
import { HttpModule, Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod, XHRBackend, RequestOptions } from '@angular/http';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    JazzDialogComponent,
    CalendarDialogComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase, {
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup,
      scope: ['user_likes']
    }),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    MaterialModule,
    FlexLayoutModule,
  ],
  providers: [
    FacebookService, 
    RestService, 
    FbBackendService, 
    AuthGuard,
    AuthService, 
    EventService,
    ProfileService,
    BaseRequestOptions,
    MockBackend, 
    {
       provide: Http,
       deps: [MockBackend, BaseRequestOptions],
       useFactory: (backend, options) => { return new Http(backend, options); }
     }
    ],
  entryComponents: [ JazzDialogComponent, CalendarDialogComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
