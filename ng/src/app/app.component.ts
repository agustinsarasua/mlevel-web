import { Component } from '@angular/core';
import {TranslateService} from 'ng2-translate/ng2-translate';
import { Router }  from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';
import { FacebookService, FacebookInitParams, FacebookLoginStatus } from 'ng2-facebook-sdk';
import { AuthService } from './services/auth.service';
import { FbBackendService } from './services/fb-backend.service';
import { Profile } from './model/profile';
import { mockProfile } from './mock-data';

import { environment } from './../environments/environment';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod, XHRBackend, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

  constructor(private backend: MockBackend, realBackend: XHRBackend, options: BaseRequestOptions, translate: TranslateService, private fb: FacebookService, public af: AngularFire, private router: Router, public authService: AuthService, public fbBackendService: FbBackendService) {
    let fbParams: FacebookInitParams = {
                                   appId: '1709613959262846',
                                   xfbml: true,
                                   version: 'v2.7'
                                   };
    this.fb.init(fbParams);


    this.backend.connections.subscribe( c => {

      let singleTicketMatcher = /\/api\/ticket\/([0-9]+)/i;        
      
      // return all tickets
      // GET: /ticket
      if (c.request.url === environment.coreBackendUrl + 'profile' && c.request.method === 0) {
        let res = new Response(new ResponseOptions({
          body: JSON.stringify(mockProfile),
          status: 200
        }));
        
        c.mockRespond(res);
      }

/*      else if(c.request.url.startsWith(environment.fbBackendUrl + 'index') && c.request.method === RequestMethod.Post) {
        let res = new Response(new ResponseOptions({
          body: '{ "result": "OK" }',
          status: 200
        }));
        
        c.mockRespond(res);
      }*/

      else {

      
      // // return ticket matching the given id
      // // GET: /ticket/:id
      // else if (c.request.url.match(singleTicketMatcher) && c.request.method === 0) {
      //   let matches = this.db.filter( (t) => {
      //     return t._id == c.request.url.match(singleTicketMatcher)[1]
      //   });
        
      //   c.mockRespond(new Response({
      //     body: JSON.stringify(matches[0])
      //   })); 
      // }
      // // Add or update a ticket
      // // POST: /ticket
      // else if (c.request.url === 'http://localhost:8080/api/ticket' && c.request.method === 1) {
      //   let newTicket: Ticket = JSON.parse(c.request._body);
        
      //   let existingTicket = this.db.filter( (ticket: Ticket) => { return ticket._id == newTicket._id});
      //   if (existingTicket && existingTicket.length === 1) {
      //     Object.assign(existingTicket[0], newTicket);
          
      //     c.mockRespond(new Response({
      //       body: JSON.stringify(existingTicket[0])
      //     }));
      //   } else {
      //     newTicket._id = parseInt(_.max(this.db, function(t) {
      //       return t._id;
      //     })._id || 0, 10) + 1 + '';
    
      //     this.db.push(newTicket);
          
      //     c.mockRespond(new Response({
      //       body: JSON.stringify(newTicket)
      //     }));
      //   }
      // }
      // // Delete a ticket
      // // DELETE: /ticket/:id
      // else if (c.request.url.match(singleTicketMatcher) && c.request.method === 3) {
      //   let ticketId = c.request.url.match(singleTicketMatcher)[1];
      //   let pos = _.indexOf(_.pluck(this.db, '_id'), ticketId);
        
      //   this.db.splice(pos, 1);
        
      //   c.mockRespond(new Response({
      //     body: JSON.stringify({})
      //   }));
      // }

        // pass through any requests not handled above
        let realHttp = new Http(realBackend, options);
        let requestOptions = new RequestOptions({
            method: c.request.method,
            headers: c.request.headers,
            body: c.request.getBody(),
            url: c.request.url,
            withCredentials: c.request.withCredentials,
            responseType: c.request.responseType
        });
        realHttp.request(c.request.url, requestOptions)
            .subscribe((response: Response) => {
                c.mockRespond(response);
            },
            (error: any) => {
                c.mockError(error);
            });

        }
      
    });

    // this.af.auth.subscribe(user => this.authenPimba(user));
    
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  // authenPimba(user):any {
  //   if(user != null) {
  //     console.log(user);
  //     this.authService.login();
  //     localStorage.setItem("pimbaUserId", user.uid);
  //     localStorage.setItem("pimbaToken", user.auth.Ed);
  //     this.fb.getLoginStatus().then(
  //       (response: FacebookLoginStatus) => {
  //         console.log(response);
  //         this.fbBackendService.indexToken(response.authResponse.accessToken, response.authResponse.userID);
  //         this.router.navigate(['/home']);
  //       },
  //       (error: any) => console.error(error)
  //     );
  //   }else{
  //     this.router.navigate(['/session/signin']);
  //   }
  // }
}
