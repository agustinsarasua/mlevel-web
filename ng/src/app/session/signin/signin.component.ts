import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AngularFire, AuthProviders } from 'angularfire2';
import { FacebookService, FacebookInitParams, FacebookLoginStatus } from 'ng2-facebook-sdk';
import { AuthService } from './../../services/auth.service';
import { FbBackendService } from './../../services/fb-backend.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private fbk: FacebookService, public af: AngularFire, public authService: AuthService, public fbBackendService: FbBackendService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )] , password: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {
    this.router.navigate ( [ '/dashboard' ] );
  }

  login(){
    this.af.auth.login().then(data => {
      console.log(data);
      this.authenPimba(data);
    }, error => {
      console.log(error);
    });
  }

  authenPimba(user):any{
    if(user != null) {
      console.log(user);
      localStorage.setItem("pimbaUserId", user.uid);
      localStorage.setItem("pimbaToken", user.auth.Pd);
      this.fbk.getLoginStatus().then(
        (response: FacebookLoginStatus) => {
          console.log(response);
          this.fbBackendService.indexToken(response.authResponse.accessToken, response.authResponse.userID);
          this.router.navigate(['/user/profile']);
        },
        (error: any) => console.error(error)
      );
    }else{
      this.router.navigate(['/session/signin']);
    }
  }
}
