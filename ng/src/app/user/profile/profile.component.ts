import { Component, OnInit, Inject } from '@angular/core';
import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';
import { ProfileService } from './../../services/profile.service';
import { Profile } from './../../model/profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  image: string;

  images: any[] = [];
  num = 1;
  profile: Profile;

  pieChartColors: any[] = [{
    backgroundColor: ['#f44336', '#3f51b5', '#ffeb3b', '#4caf50', '#2196f']
  }];

  pieOptions: any = {
    responsive: true,
    legend: {
      position: 'right'
    }
  };

  pieChartLabels: string[] = ['MS Word', 'Typing', 'Sage Pastel'];
  pieChartData: number[] = [300, 500, 100];
  pieChartType = 'pie';

  constructor(private profileService: ProfileService, @Inject(FirebaseApp) firebaseApp: firebase.app.App) {

    const storageRef = firebaseApp.storage().ref().child('photos').child('Qe5NWg5dxITrvWCeKLyO1T6Kud93').child('profile-picture');
    storageRef.getDownloadURL().then(url => this.image = url);
//console.log(profile);
    for (this.num; this.num <= 9; this.num += 1) {
      this.images.push(this.num);
    }
  }

  ngOnInit() {
    this.profileService.loadUserProfile().then(profile => {
      console.log(profile);
      this.profile = profile;
    });
  }

}
